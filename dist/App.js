"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _DataTables = _interopRequireDefault(require("./DataTables"));

var _examples = require("./examples");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var App = function App() {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "app"
  }, /*#__PURE__*/_react.default.createElement(_DataTables.default, {
    labels: _examples.exampleLabels,
    data: _examples.exampleData
  }));
};

var _default = App;
exports.default = _default;