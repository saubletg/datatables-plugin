# DataTables plugin for React
DataTables is a table enhancing plug-in for React, adding sorting, paging and filtering abilities to plain HTML tables with minimal effort. 

## Features
+ Pagination with different number of displayed entries (10 / 25 / 50 / 100)
+ Searching accross all entries
+ Sorting by columns (ASC or DESC)
+ Dynamic information about number of displayed data

## Start
You need to install :
datatables-plugin

## Requirements
React 17.0.2+

## Installation
+ using NPM npm install datatables-plugin

+ using yarn yarn add datatables-plugin

## Usage
To use the plugin, import it in your React component. 

It needs 2 props to work: labels and data
```javascript
MyComponent.js

import DataTables from "datatables-plugin";
import { labels, data } from "./examples";

const MyComponent = () => {
  <DataTables labels={labels} data={data} />;
};
```
```javascript
examples.js

const exampleLabels = [
  { text: "First Name", value: "firstName" },
  { text: "Last Name", value: "lastName" },
  { text: "Start Date", value: "startDate" },
  { text: "Department", value: "department" },
  { text: "Date of Birth", value: "dateOfBirth" },
  { text: "Street", value: "street" },
  { text: "City", value: "city" },
  { text: "State", value: "state" },
  { text: "Zip Code", value: "zipCode" },
];

const exampleData = [
  {
    firstName: "John",
    lastName: "Doe",
    startDate: "04/14/2021",
    department: "Marketing",
    dateOfBirth: "01/01/1990",
    street: "Main Street",
    city: "NY",
    state: "AL",
    zipCode: "01800",
  },
  {
    firstName: "Elisa",
    lastName: "Tyrel",
    startDate: "03/02/2020",
    department: "Marketing",
    dateOfBirth: "01/25/1988",
    street: "Helia Street",
    city: "San Francisco",
    state: "AK",
    zipCode: "85699",
  },
];

export { exampleLabels, exampleData };
```

## API
+ DataTable needs 2 props:

+ data that contains the data for the rows
+ labels that defines the name of the columns
+ The keys of each object element in the data array should be the same as the value keys of each label element of the labels array.

## DataTable props

+ data array of objects containing a single row
+ labels array of objects with the labels of the columns
data

```javascript
[
  {
    columnName1: "text1",
    columnName2: "text2",
    columnName3: "text3",
  },
  {
    columnName1: "text1",
    columnName2: "text2",
    columnName3: "text3",
  },
];
labels

[
  { text: "Column Name 1", value: "columnName1" },
  { text: "Column Name 2", value: "columnName2" },
  { text: "Column Name 3", value: "columnName3" },
];
```